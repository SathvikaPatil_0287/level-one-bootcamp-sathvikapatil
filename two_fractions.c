//WAP to find the sum of two fractions.
#include<stdio.h>
typedef struct
{
  int n;
  int d;
}Fract;
Fract compute_sum(Fract f1,Fract f2);
void print_sum(Fract f1,Fract f2,Fract s);
Fract input_Fract()
{
  Fract x;
  printf("\nEnter the numerator and denominator");
  scanf("%d%d",&x.n,&x.d);
  return x;
}
int main()
{
  Fract f1,f2;
  f1=input_Fract();
  f2=input_Fract();
  Fract s=compute_sum(f1,f2);
  print_sum(f1,f2,s);
}
int gcd(int n,int d)
{
  int temp;
  while(n!=0)
  {
    temp=n;
    n=d%n;
    d=temp;
  }
  return d;
}
Fract compute_sum(Fract f1,Fract f2)
{
  Fract s;
  int num,den;
  num=((f1.n* f2.d) + (f2.n * f1.d));
  den=(f1.d * f2.d);
  int g=gcd(num,den);
  s.n=num/g;
  s.d=den/g;
  return s;
}
void print_sum(Fract f1,Fract f2,Fract s)
{
  printf("\nThe sum of %d/%d and %d/%d is %d/%d",f1.n,f1.d,f2.n,f2.d,s.n,s.d);
}