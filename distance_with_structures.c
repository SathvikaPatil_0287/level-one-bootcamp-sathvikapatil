//WAP to find the distance between two points using structures and 4 functions.
#include<stdio.h> 
#include<math.h> 
struct points
{ 
int x, y; 
}; 
float distance(struct points a, struct points b) 
{ 
float dis; 
dis=sqrt((pow(a.x-b.x,2)) + (pow(a.y-b.y,2))); 
return dis;
} 
int main() 
{ 
struct points a,b; 
printf("Enter the x & y coordinates of point a:\n"); 
scanf("%d %d", &a.x, &a.y); 
printf("Enter the x & y coordinates of point b:\n"); 
scanf("%d %d", &b.x, &b.y); 
printf("Distance between the two points a and b is:%f\n",distance(a,b)); 
return 0; 
}