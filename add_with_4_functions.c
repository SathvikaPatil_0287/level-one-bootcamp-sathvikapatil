//Write a program to add two user input numbers using 4 functions.
#include <stdio.h>
int input()
{
  int n;
  printf("\n enter the number:");
  scanf("%d",&n);
  return n;
}
int sum(int a,int b)
{
  int sum;
  sum=a+b;
  return sum;
}
void output(int n)
{
  printf("\n the sum is %d",n);
}
int main()
{
  float x,y,z;
  x=input();
  y=input();
  z=sum(x,y);
  output(z);
  return 0;
}