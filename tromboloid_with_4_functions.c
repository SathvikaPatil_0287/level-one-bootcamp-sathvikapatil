//WAP to find the volume of a tromboloid using 4 functions.
#include <stdio.h>
float input()
{
  float n;
  printf("enter the number:\n");
  scanf("%f",&n);
  return n;
}
float vol(float h,float d,float b)
{
  float vol;
  vol=(((h*d*b)+(d/b))/3);
  return vol;
}
void output(float n)
{
  printf("volume of tromboloid is %2f\n",n);
}
int main()
{
  float w,x,y,z;
  w=input();
  x=input();
  y=input();
  z=vol(w,x,y);
  output(z);
  return 0;
}